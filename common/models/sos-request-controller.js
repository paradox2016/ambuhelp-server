module.exports = function(SOSRequestController) {
    var loopback = require('loopback');
    var MapUtil = require('./mapUtility.js').MapUtil;
    var PubSub = require('../../server/pubsub.js');

    var driverList = [];

    var userList = [];

    var hospitalList = [];

    var userTypeObj = {
        driver:'driver',
        user:'user',
        hospital:'hospital'
    }

    var updateDriverStatus = function(id,status){
        driverList.forEach(function (value,index) {
           if(value.id == id){
               driverList[index].status=status;
           }
        });
    }

    function findClosestHospital(location){
        var userLocation = new loopback.GeoPoint(location);
        // Get list of waiting drivers
        var nearestHospitalList = [];
        hospitalList.forEach(function(hospital){
          if(hospital.id && hospital.location && hospital.location.lat && hospital.location.lng){
            nearestHospitalList.push(hospital);
          }

        });
        // Sort Driver List
        nearestHospitalList.sort(function(driverA,driverB){
            if(driverA.location.lat && !driverA.location.lat){
                return 1;
            }
            else if(!driverA.location.lat && driverA.location.lat){
                return -1;
            } else{
                return (userLocation.distanceTo(driverA.location) - userLocation.distanceTo(driverB.location));
            }

        });
        if(nearestHospitalList.length > 0 ){
            return nearestHospitalList[0];
        }
        return null;


    }

    function findNearestAmbulance(location){
        var userLocation = new loopback.GeoPoint(location);

        var nearestAmulanceList = [];
        driverList.forEach(function(driver){
          if(driver.id && driver.location && driver.location.lat && driver.location.lng && driver.status==='waiting'){
              nearestAmulanceList.push(Object.assign({}, driver));
          }
        });

        // Sort Driver List
        nearestAmulanceList.sort(function(driverA,driverB){
            if(driverA.location.lat && !driverA.location.lat){
                return 1;
            }
            else if(!driverA.location.lat && driverA.location.lat){
                return -1;
            } else{
                return (userLocation.distanceTo(driverA.location) - userLocation.distanceTo(driverB.location));
            }

        });
        if(nearestAmulanceList.length > 0 ){
            return nearestAmulanceList[0];
        }
        return null;

    }

    SOSRequestController.raiseSOSRequest = function(user,cb){
        var bookingRequest = {
            "user": user,
            "hospital": null,
            "ambulance": null
        };
        var socket = SOSRequestController.app.io;
        /*findClosestHospital(user.location);

        HospitalModel.findById({id:})*/
        var HospitalModel = SOSRequestController.app.models.HospitalModel;
        var AmbulanceModel = SOSRequestController.app.models.AmbulanceModel;
        var SOSRequestModel = SOSRequestController.app.models.SOSRequestModel;
        var ambulance = findNearestAmbulance(user.location);
        var hospital = findClosestHospital(user.location);
        if(ambulance!=null){
            AmbulanceModel.findById(ambulance.id,{ include: "driverDetails"},function(err,ambulanceObj){
                if(err){
                    ambulance.status = 'waiting';
                    cb("No Ambulance Found.",null);

                } else {
                    ambulanceObj.location=  ambulance.location;
                    bookingRequest.ambulance = ambulanceObj;
                    HospitalModel.findById(hospital.id,{include:"hospitalDetails"},function(err,hospitalObj){
                        if(err){
                            ambulance.status = 'waiting';
                            cb("No Hospital Found.",null);

                        } else {
                            bookingRequest.hospital = hospitalObj;
                            SOSRequestModel.create(bookingRequest,function(err,bookingRequest){
                                if(err){
                                    ambulance.status = 'waiting';
                                    cb("Booking Failed",null);
                                } else{
                                    updateDriverStatus(ambulance.id,'busy');
                                    PubSub.publish(socket,{
                                        collectionName:'SOSRequest',
                                        modelId: bookingRequest.ambulance.id,
                                        data:bookingRequest,
                                        method:'POST'
                                    });

                                    PubSub.publish(socket,{
                                        collectionName:'SOSRequest',
                                        modelId: bookingRequest.hospital.id,
                                        data:bookingRequest,
                                        method:'POST'
                                    });

                                    cb(null,bookingRequest);
                                }

                            });
                        }
                    });
                }
            })
        } else {
            cb("No Ambulance Found.",null);
        }

    }

    SOSRequestController.remoteMethod('raiseSOSRequest', {
        accepts: {arg: 'user', type: 'object',http: { source: 'body' }},
        returns: {arg: 'bookingRequest', type: 'object'},
        http: {path: '/raiseSOSRequest', verb: 'post'}
    });



    SOSRequestController.getUserList = function(userType,cb){
        switch(userType) {
            case userTypeObj.driver: {
                return cb(null, driverList);
            }
            case userTypeObj.user: {
                return cb(null, userList);
            }
            case userTypeObj.hospital: {
                return cb(null, hospitalList);
            }
            default: {
                var arr = driverList.concat(userList);
                arr = arr.concat(hospitalList);
                return cb(null, arr);
            }
        }
    }

    SOSRequestController.remoteMethod('getUserList', {
            http: { path: '/getuserlist', verb: 'get'},
            accepts:{arg: 'userType',type: 'string' },
            returns: {arg: 'List',type: 'array'}
        }
    );

    SOSRequestController.addUser = function(id,userType,cb){

        switch(userType){
            case userTypeObj.driver:{
                var found = false;
                driverList.forEach(function (value) {
                    if(value.id===id){
                        found = true;
                    }
                });
                  // Prevent Null Pointer
                if(!found && id){
                    var driver = {
                        id:id,
                        location:{},
                        status:'waiting',
                        type:'driver'
                    }
                    driverList.push(driver);
                }
                return cb(null,driverList);
            }
            case userTypeObj.user:{
                var found = false;
                userList.forEach(function (value) {
                    if(value.id===id){
                        found = true;
                    }
                });
                // Prevent Null Pointer
                if(!found && id){
                    var user = {
                        id:id,
                        location:{},
                        type:'user'
                    }
                    userList.push(user);
                }
                return cb(null,userList);
            }
            case userTypeObj.hospital:{
                var found = false;
                hospitalList.forEach(function (value) {
                    if(value.id===id){
                        found = true;
                    }
                });

                if(!found && id){
                    var hospital = {
                        id:id,
                        location:{},
                        type:'hospital'
                    }
                    hospitalList.push(hospital);
                }
                return cb(null,hospitalList);
            }
        }

        return cb(null,null);


    }

    SOSRequestController.remoteMethod('addUser', {
            http: {path: '/adduser',verb: 'post'},
            accepts: [
                {arg: 'id',type: 'string' },
                {arg: 'userType',type: 'string'}
            ],
            returns: {arg: 'List',type: 'array'}
        }
    );

    SOSRequestController.removeUser = function(id,userType,cb){
        switch(userType){
            case userTypeObj.driver:{
              if(id){
                driverList.forEach(function (value,index) {
                    if(value.id===id){
                        driverList.splice(index,1);
                    }
                });
              }
              else {
                driverList=[];
              }
                return cb(null,driverList);
            }
            case userTypeObj.user:{
              if(id){
                userList.forEach(function (value,index) {
                      if(value.id===id){
                          userList.splice(index,1);
                      }
                  });
                }
                else{
                  userList=[];
                }
                return cb(null,userList);
            }
            case userTypeObj.hospital:{
              if(id){
                hospitalList.forEach(function (value,index) {
                    if(value.id===id){
                        hospitalList.splice(index,1);
                    }
                });
              }
                else{
                  hospitalList=[];
                }
                return cb(null,hospitalList);
            }
            default:{
              hospitalList=[];
              driverList =[];
              userList=[];
              return cb(null,[]);
            }
        }


    }

    SOSRequestController.remoteMethod('removeUser', {
            http: {path: '/removeuser',verb: 'delete'},
            accepts: [
                {arg: 'id', type: 'string'},
                {arg: 'userType',type: 'string'}
            ],
            returns: { arg: 'List',type: 'array'}
        }
    );


    SOSRequestController.updateLocation = function(userParams,cb){
        switch(userParams.userType){
            case userTypeObj.driver:{
                driverList.forEach(function (value) {
                    if(value.id===userParams.id && (userParams.location) && (userParams.location.lat) && (userParams.location.lng)){
                        value.location= userParams.location;
                    }
                });
                return cb(null,driverList);
            }
            case userTypeObj.user:{
                userList.forEach(function (value) {
                    if(value.id===userParams.id && (userParams.location) && (userParams.location.lat) && (userParams.location.lng)){
                        value.location= userParams.location;
                    }
                });
                return cb(null,userList);
            }
            case userTypeObj.hospital:{
                hospitalList.forEach(function (value) {
                    if(value.id===userParams.id && (userParams.location) && (userParams.location.lat) && (userParams.location.lng)){
                        value.location= userParams.location;
                    }
                });
                return cb(null,hospitalList);
            }
        }
        return cb(null,[]);


    }

    SOSRequestController.remoteMethod('updateLocation', {
            http: {path: '/updatelocation',verb: 'post'},
            accepts: [
                {arg: 'userParams',type: 'object' }
            ],
            returns: {arg: 'List',type: 'array'}
        }
    );


    SOSRequestController.findNearByAmbulance = function(mapParams, cb) {

        if(mapParams.coordinates.lat && mapParams.coordinates.lng){
            if(mapParams.coordinates.lat<-90 || mapParams.coordinates.lat>90){
                cb('Invalid Coordinates: Latitude out of range [-90,90]', null);
            }
            else if(mapParams.coordinates.lng<-180 || mapParams.coordinates.lng>180){
                cb('Invalid Coordinates: Longitude out of range [-180,180]', null);
            } else {
                var nearByAmbulance = [];


                var inputLocation = new loopback.GeoPoint(mapParams.coordinates);
                var radius = 5000; // 5 KM radius
                if(mapParams.coordinates.radius){
                    radius = mapParams.coordinates.radius;
                }

                nearByAmbulance = driverList.filter(function(currentValue){
                    if(currentValue.location.lat){
                        return (inputLocation.distanceTo(currentValue.location, {type: 'meters'})<=radius);
                    }
                    return false;
                });

                var northBound = MapUtil(mapParams.coordinates.lat,mapParams.coordinates.lng,5.0,360);
                var eastBound = MapUtil(mapParams.coordinates.lat,mapParams.coordinates.lng,5.0,90);
                var southBound = MapUtil(mapParams.coordinates.lat,mapParams.coordinates.lng,5.0,180);
                var westBound = MapUtil(mapParams.coordinates.lat,mapParams.coordinates.lng,5.0,270);

                var mapBound = {
                    latMin:southBound.location.lat,
                    latMax:northBound.location.lat,
                    lngMin:westBound.location.lng,
                    lngMax:eastBound.location.lng
                }

                // Create dummy markers in range 5 to 15 plus actual nearby ambulance
                var dummyMapMarkerSize = Math.floor(Math.random() * 10) + 2;

                for(var i=0;i<dummyMapMarkerSize;i++){
                    var currentValue = {
                        location:{
                            lat:null,
                            lng:null
                        }
                    }
                    do{
                        currentValue.location.lat = (Math.random() * (mapBound.latMax - mapBound.latMin) +mapBound.latMin);
                        currentValue.location.lng = (Math.random() * (mapBound.lngMax - mapBound.lngMin) +mapBound.lngMin);
                    }while((inputLocation.distanceTo(currentValue.location, {type: 'meters'})>radius))

                    nearByAmbulance.push(currentValue);

                }

                nearByAmbulance.sort(function(driverA,driverB){
                    if(driverA.location.lat && !driverA.location.lat){
                        return 1;
                    }
                    else if(!driverA.location.lat && driverA.location.lat){
                        return -1;
                    } else{
                        return (inputLocation.distanceTo(driverA.location) - inputLocation.distanceTo(driverB.location));
                    }

                })

                cb(null, nearByAmbulance);

            }
        } else {
            cb('Invalid Coordinates', null);
        }

    }

    SOSRequestController.remoteMethod('findNearByAmbulance', {
        accepts: {arg: 'mapParams', type: 'object',http: { source: 'body' }},
        returns: {arg: 'nearByAmbulance', type: 'array'},
        http: {path: '/nearbyambulance', verb: 'post'}
    });


    SOSRequestController.updateDriverLocation = function(driverLocation,cb) {
        var socket = SOSRequestController.app.io;
        PubSub.publish(socket, {
            collectionName: 'LocationUpdateServer',
            method: 'PATCH',
            modelId: driverLocation.id,
            data: driverLocation.location
        });
        cb(null,null);
    }
    SOSRequestController.remoteMethod('updateDriverLocation', {
        accepts: {arg: 'driverLocation', type: 'object',http: { source: 'body' }},
        http: {path: '/updateDriverLocation', verb: 'post'}
    });


    SOSRequestController.updateRequestStatus = function(data,cb) {
        var socket = SOSRequestController.app.io;
        var SOSRequestModel = SOSRequestController.app.models.SOSRequestModel;

        if(data.type==='pickup-update'){
            SOSRequestModel.findById(data.id,function(err,sosRequestModel){
                if(err){
                    console.log(err);
                } else {

                    sosRequestModel.updateAttributes({emergencyData:data.data},function(err,success) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("Pickup update Called");
                            PubSub.publish(socket,{
                                collectionName:'SOSPickupUpdate',
                                modelId: data.id,
                                data:data,
                                method:'UPDATE'
                            });
                        }
                    });

                }
            });
        } else if(data.type==='request-complete'){
            SOSRequestModel.findById(data.id,function(err,sosRequestModel){
                if(err){
                    console.log(err);
                } else {
                    var currentDate = new Date();
                    var ambulanceId = sosRequestModel.ambulance.id;
                    updateDriverStatus(ambulanceId,'waiting');
                    sosRequestModel.updateAttributes({state:'closed', completionTimestamp: currentDate},function (err,success) {
                        if(err){
                            console.log(err);
                        } else{
                            //console.log(success)
                            PubSub.publish(socket,{
                                collectionName:'SOSRequestComplete',
                                modelId: data.id,
                                data:success?true:false,
                                method:'UPDATE'
                            });
                        }
                    })
                }
            });
        }
        cb(null,null);
    }
    SOSRequestController.remoteMethod('updateRequestStatus', {
        accepts: {arg: 'data', type: 'object',http: { source: 'body' }},
        http: {path: '/updateRequestStatus', verb: 'post'}
    });
};
