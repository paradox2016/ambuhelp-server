// Copyright 1997 Ed Williams All rights reserved

function ComputeFormDir(latitude,longitude,distance,degree){
    //get select  values
    var dc;
    var lat1, lon1;
    var d12, crs12;
    d12 = distance;
    /* Input and validate data */
    lat1 = (Math.PI / 180) * 1 * checkField(latitude,"lat");
    lon1 = (Math.PI / 180) * -1 * checkField(longitude,"lon");

    /* Always KM */
    dc = dconv(2); /* get distance conversion factor */
    d12 /= dc;  // in nm

    crs12 = degree * Math.PI / 180.0;  // radians
    //console.log("lat1=" + lat1 + " lon1=" + lon1 + " d12=" + d12 + " crs12=" + crs12);

    /* Always Select International Model */
    ellipse = getEllipsoid(4); //get ellipse
    /*if (ellipse.name=="Sphere"){
        // spherical code
        d12 /= (180 * 60 / Math.PI);  // in radians
        cd = direct(lat1, lon1, crs12, d12);
        lat2 = cd.lat * (180 / Math.PI);
        lon2 =cd.lon*(180/Math.PI)
    } else {*/
        // elliptic code
        var outputCode = direct_ell(lat1, -lon1, crs12, d12, ellipse);  // ellipse uses East negative
        lat2 = outputCode.lat * (180 / Math.PI);
        lon2 = -1*outputCode.lon * (180 / Math.PI);
        crs21 = outputCode.crs21;
        // ellipse uses East negative
   /* }*/

  /*  console.log("d=" + d + "  crs12=" + crs12 + "   crs21=" + crs21);*/


    lat2s = Math.abs(lat2);
    lon2s = Math.abs(lon2);
    return {
        location: {
            lat: lat2s,
            lng: lon2s
        }
    };
}


function direct(lat1,lon1,crs12,d12) {
    var EPS = 0.00000000005;
    var dlon, lat, lon;
// 5/16 changed to "long-range" algorithm
    if ((Math.abs(Math.cos(lat1))<EPS) && !(Math.abs(Math.sin(crs12))<EPS)){
        console.log("Only N-S courses are meaningful, starting at a pole!");
    }

    lat = Math.asin(Math.sin(lat1) * Math.cos(d12) +
        Math.cos(lat1) * Math.sin(d12) * Math.cos(crs12));
    if (Math.abs(Math.cos(lat))<EPS){
        lon=0.0; //endpoint a pole
    }else{
        dlon = Math.atan2(Math.sin(crs12) * Math.sin(d12) * Math.cos(lat1),
            Math.cos(d12) - Math.sin(lat1) * Math.sin(lat));
        lon = mod(lon1 - dlon + Math.PI, 2 * Math.PI) - Math.PI;
    }
    //console.log("lat1="+lat1+" lon1="+lon1+" crs12="+crs12+" d12="+d12+" lat="+lat+" lon="+lon)
    out = {
        lat: lat,
        lon: lon
    }
    return out;
}

function direct_ell(glat1,glon1,faz,s,ellipse) {
// glat1 initial geodetic latitude in radians N positive
// glon1 initial geodetic longitude in radians E positive
// faz forward azimuth in radians
// s distance in units of a (=nm)

    var EPS = 0.00000000005;
    var r, tu, sf, cf, b, cu, su, sa, c2a, x, c, d, y, sy, cy, cz, e;
    var glat2, glon2, baz, f;

    if ((Math.abs(Math.cos(glat1))<EPS) && !(Math.abs(Math.sin(faz))<EPS)){
        console.log("Only N-S courses are meaningful, starting at a pole!");
    }

    a = ellipse.a;
    f = 1 / ellipse.invf;
    r = 1 - f;
    tu = r * Math.tan(glat1);
    sf = Math.sin(faz);
    cf = Math.cos(faz);
    if (cf==0){
        b = 0.0;
    }else{
        b = 2.0 * atan2(tu, cf);
    }
    cu = 1.0 / Math.sqrt(1 + tu * tu);
    su = tu * cu;
    sa = cu * sf;
    c2a = 1 - sa * sa;
    x = 1.0 + Math.sqrt(1.0 + c2a * (1.0 / (r * r) - 1.0));
    x = (x - 2.0) / x;
    c = 1.0 - x;
    c = (x * x / 4.0 + 1.0) / c;
    d = (0.375 * x * x - 1.0) * x;
    tu = s / (r * a * c);
    y = tu;
    c = y + 1;
    while (Math.abs (y - c) > EPS)
    {
        sy = Math.sin(y);
        cy = Math.cos(y);
        cz = Math.cos(b + y);
        e = 2.0 * cz * cz - 1.0;
        c = y;
        x = e * cy;
        y = e + e - 1.0;
        y = (((sy * sy * 4.0 - 3.0) * y * cz * d / 6.0 + x) * d / 4.0- cz) * sy * d + tu;
    }

    b = cu * cy * cf - su * sy;
    c = r * Math.sqrt(sa * sa + b * b);
    d = su * cy + cu * sy * cf;
    glat2 = modlat(atan2(d, c));
    c = cu * cy - su * sy * cf;
    x = atan2(sy * sf, c);
    c = ((-3.0 * c2a + 4.0) * f + 4.0) * c2a * f / 16.0;
    d = ((e * cy * c + cz) * sy * c + y) * sa;
    glon2 = modlon(glon1 + x - (1.0- c) * d * f);	// fix date line problems
    baz = modcrs(atan2(sa, b) + Math.PI);

    var out = {
        lat:glat2,
        lon:glon2,
        crs21:baz
    };
    return out;
}



//***************Utility***************

function MakeArray(n){
    this.length = n;
    for (var i=1;i<=n;i++){
        this[i] = 0;
    }
    return this;
}

function dconv(selection){
    dc=new MakeArray(3);
    dc[1]=1.0;
    dc[2]=1.852;//km
    dc[3]=185200.0/160934.40; // 1.150779448 sm
    dc[4]=185200.0/30.48; // 6076.11549  //ft
    return dc[selection];
}

function ellipsoid(name, a, invf){
    /* constructor */
    this.name = name;
    this.a = a;
    this.invf = invf;
}

function getEllipsoid(selection){
    no_selections = 10;
    ells = new MakeArray(no_selections);
    ells[1] = new ellipsoid("Sphere", 180 * 60 / Math.PI, "Infinite"); // first one
    ells[2] = new ellipsoid("WGS84", 6378.137 / 1.852, 298.257223563);
    ells[3] = new ellipsoid("NAD27", 6378.2064 / 1.852, 294.9786982138);
    ells[4] = new ellipsoid("International", 6378.388 / 1.852, 297.0);
    ells[5] = new ellipsoid("Krasovsky", 6378.245 / 1.852, 298.3);
    ells[6] = new ellipsoid("Bessel", 6377.397155 / 1.852, 299.1528);
    ells[7] = new ellipsoid("WGS72", 6378.135 / 1.852, 298.26);
    ells[8] = new ellipsoid("WGS66", 6378.145 / 1.852, 298.25);
    ells[9] = new ellipsoid("FAI sphere", 6371.0 / 1.852, 1000000000.0);
    return ells[selection]
}


function checkField(field,type){
    latlon=field;
    if (type==="lat") {
        if (latlon > 90.0) {
            console.log("Latitudes cannot exceed 90 degrees");
        }
    }
    if (type==="lon") {
        if (latlon > 180.0) {
            console.log("Longitudes cannot exceed 180 degrees");
        }
    }
    return latlon;
}


function atan2(y,x){
    var out;
    if (x <0) {
        out = Math.atan(y / x) + Math.PI;
    }
    if ((x >0) && (y>=0)){
        out = Math.atan(y / x);
    }
    if ((x >0) && (y<0)) {
        out = Math.atan(y / x) + 2 * Math.PI;
    }
    if ((x==0) && (y>0)) {
        out = Math.PI / 2;
    }
    if ((x==0) && (y<0)) {
        out = 3 * Math.PI / 2;
    }
    if ((x==0) && (y==0)) {
        console.log("atan2(0,0) undefined");
        out = 0.0;
    }
    return out;
}

function mod(x,y){
    return x-y*Math.floor(x/y)
}

function modlon(x){
    return mod(x+Math.PI,2*Math.PI)-Math.PI
}

function modcrs(x){
    return mod(x,2*Math.PI)
}

function modlat(x){
    return mod(x+Math.PI/2,2*Math.PI)-Math.PI/2
}

exports.MapUtil = ComputeFormDir;

