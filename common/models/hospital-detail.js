module.exports = function(Hospitaldetail) {
    var loopback = require('loopback');
    Hospitaldetail.findNearByHospitals = function(mapParams, cb) {
        if(mapParams.coordinates.lat && mapParams.coordinates.lng){
            if(mapParams.coordinates.lat<-90 || mapParams.coordinates.lat>90){
                cb('Invalid Coordinates: Latitude out of range [-90,90]', null);
            }
            else if(mapParams.coordinates.lng<-180 || mapParams.coordinates.lng>180){
                cb('Invalid Coordinates: Longitude out of range [-180,180]', null);
            } else {
                var nearByHospitalList = [];

                var inputLocation = new loopback.GeoPoint(mapParams.coordinates);
                var radius = 5000; // 5 KM radius
                if(mapParams.coordinates.radius){
                    radius = mapParams.coordinates.radius;
                }
                Hospitaldetail.find({ fields: {id: true, hospitalModelId: true, mapLocation: true,name:true} },function(err,hospitalDetails){
                    if(err){
                        cb(err, null);
                    } else{
                        nearByHospitalList = hospitalDetails.filter(function(currentValue){
                            return (inputLocation.distanceTo(currentValue.mapLocation, {type: 'meters'})<=radius);
                        });

                        nearByHospitalList.sort(function(hospitalA,hospitalB){
                            return (inputLocation.distanceTo(hospitalA.mapLocation) - inputLocation.distanceTo(hospitalB.mapLocation));
                        })
                        cb(null, nearByHospitalList);
                    }
                });

            }
        } else {
            cb('Invalid Coordinates', null);
        }

    }

    Hospitaldetail.remoteMethod('findNearByHospitals', {
        accepts: {arg: 'mapParams', type: 'object',http: { source: 'body' }},
        returns: {arg: 'nearbyHospitals', type: 'array'},
        http: {path: '/nearbyHospitals', verb: 'post'}
    });

};
