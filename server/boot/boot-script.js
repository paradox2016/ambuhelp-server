/**
 * Created by
 * @author Saurabh Dutta <saurabhdutta.jk@gmail.com>
 * Date :  11/15/2016.
 */

module.exports = function(app) {
    delete app.models.HospitalModel.validations.email;
    delete app.models.AmbulanceModel.validations.email;
};