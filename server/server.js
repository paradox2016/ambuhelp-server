var loopback = require('loopback');
var boot = require('loopback-boot');
var helmet = require('helmet');
var pubsub = require('./pubsub.js');

var app = module.exports = loopback();

// boot scripts mount components like REST API
boot(app, __dirname);
app.use(helmet());
app.start = function () {
    // start the web server
    return app.listen(process.env.PORT || 3000, function () {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
    });
};

// start the server if `$ node server.js`
if (require.main === module) {
    //app.start();
    app.io = require('socket.io')(app.start());

    app.io.on('connection', function(socket){
        var userId = null;
        var userType = null;
        socket.on('new-user',function (value) {
            userId = value.id;
            userType = value.type;
            app.models.SOSRequestController.addUser(userId,userType,function () {
                console.log(userType+ " is connected having id "+ userId);
                socket.emit('new-user-connected');
            })
        });
        socket.on('update-location',function (value) {

            app.models.SOSRequestController.updateLocation(value,function(){
                //console.log(userType+ " with id : " + userId + " updated it's location ");
            });
        });

        socket.on('disconnect', function(){
            app.models.SOSRequestController.removeUser(userId,userType,function(){
                console.log(userId + ' is disconnected of type '+userType);
            });
        });

        pubsub.subscribe(socket,{
            collectionName: 'LocationUpdate',
            method:'POST',
        },function (driverData) {
            app.models.SOSRequestController.updateDriverLocation(driverData,function(){});
        });

        pubsub.subscribe(socket,{
            collectionName: 'SOSRequestUpdate',
            method:'POST',
        },function (data){
            app.models.SOSRequestController.updateRequestStatus(data,function(){});
        });

    });
}
